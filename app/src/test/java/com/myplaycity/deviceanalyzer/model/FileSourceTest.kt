package com.myplaycity.deviceanalyzer.model

import org.junit.After
import org.junit.Test

import org.junit.Assert.*
import org.junit.Before
import java.io.File
import java.util.*

/**
 * Created by i.chiruhin@myplaycity.com on 1/7/16.
 */
class FileSourceTest {

    lateinit var testDir: File

    @Before
    fun setUp() {
        testDir = File(File.createTempFile("test_", null).parentFile.absolutePath, "" + UUID.randomUUID())
        testDir.mkdirs()

        File(testDir, "dir-11/dir-21/dir-31").mkdirs()
        File(testDir, "dir-11/dir-22").mkdirs()
        File(testDir, "dir-12").mkdirs()

        File(testDir, "1000.txt").writeBytes(ByteArray(1000))
        File(testDir, "dir-11/2000.txt").writeBytes(ByteArray(2000))
        File(testDir, "dir-12/3000.txt").writeBytes(ByteArray(3000))

        File(testDir, "dir-11/dir-21/4000.txt").writeBytes(ByteArray(4000))
        File(testDir, "dir-11/dir-21/5000.txt").writeBytes(ByteArray(5000))
        File(testDir, "dir-11/dir-21/dir-31/6000.txt").writeBytes(ByteArray(6000))
    }

    @After
    fun tearDown() {
        testDir.deleteRecursively()
    }

    @Test
    fun testGetFileSource() {
        val actual = FileSource().getFileSource(testDir).take(20).toList().toBlocking().single()

        val expected = listOf(
            makeRecord("", 1000),
            makeRecord("dir-11/", 2000),
            makeRecord("dir-12/", 3000),
            makeRecord("dir-11/dir-21/", 4000),
            makeRecord("dir-11/dir-21/", 5000),
            makeRecord("dir-11/dir-21/dir-31/", 6000))

        assertEquals(expected.sortedBy { it.path }, actual.sortedBy { it.path })
    }

    fun makeRecord(path: String, size: Long): Record {
        return Record(File(testDir, "$path$size.txt").absolutePath, (1 + size / 4096) * 4096)
    }
}