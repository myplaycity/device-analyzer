package com.myplaycity.deviceanalyzer.common

import android.support.v4.view.ViewCompat
import android.support.v4.view.ViewPropertyAnimatorCompat
import android.view.View

/**
 * Created by igor on 13/01/16.
 */
fun View.animateCompat(): ViewPropertyAnimatorCompat {
    return ViewCompat.animate(this)
}