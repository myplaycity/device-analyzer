package com.myplaycity.deviceanalyzer.common

import android.os.Handler
import android.os.Looper
import rx.Observable
import rx.Scheduler
import rx.schedulers.Schedulers

/**
 * Created by i.chiruhin@myplaycity.com on 1/6/16.
 */

// TODO: удалить initValue
fun <T> Observable<T>.filterByPrev(initValue: T, filter: (T, T) -> Boolean): Observable<T> {
    return scan(initValue to initValue) { p, s -> Pair(p.second, s) }
        .filter { filter(it.first, it.second) }
        .map { it!!.second }
}

fun <T> Observable<T>.subscribeComplete(func: (Throwable?) -> Unit) {
    subscribe({ }, {
        it.printStackTrace()
        func(it)
    }, { func(null) })
}

fun <T> Observable<T>.toSingleUnit(): Observable<Unit> {
    return map { Unit }.last()
}

fun MainScheduler(): Scheduler {
    return Schedulers.from { Handler(Looper.getMainLooper()).post(it) }
}