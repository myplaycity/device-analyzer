package com.myplaycity.deviceanalyzer.common

/**
 * Created by i.chiruhin@myplaycity.com on 1/8/16.
 */

fun String.startsWithAny(vararg prefix: String): Boolean {
    return prefix.any { startsWith(it) }
}