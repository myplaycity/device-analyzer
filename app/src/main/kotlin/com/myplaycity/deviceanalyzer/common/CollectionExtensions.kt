package com.myplaycity.deviceanalyzer.common

/**
 * Created by i.chiruhin@myplaycity.com on 1/8/16.
 */

fun <T> List<T>.orDefault(vararg default: T): List<T> {
    return if (isEmpty()) default.toList() else this;
}