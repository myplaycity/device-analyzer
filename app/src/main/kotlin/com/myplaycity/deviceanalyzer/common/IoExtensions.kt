package com.myplaycity.deviceanalyzer.common

import rx.Observable
import java.io.InputStream

/**
 * Created by i.chiruhin@myplaycity.com on 1/7/16.
 */

fun InputStream.lines(): Observable<String> {
    return Observable.create { subscriber ->
        try {
            bufferedReader().useLines {
                it.forEach { subscriber.onNext(it) }
            }
            subscriber.onCompleted()
        } catch (e: Exception) {
            subscriber.onError(e)
        }
    }
}