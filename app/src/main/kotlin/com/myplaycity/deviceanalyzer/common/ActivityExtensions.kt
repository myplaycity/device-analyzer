package com.myplaycity.deviceanalyzer.common

import android.Manifest
import android.app.Activity
import android.content.pm.PackageManager
import android.os.Build
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat

/**
 * Created by i.chiruhin@myplaycity.com on 1/6/16.
 */

fun Activity.showSnackbar(resId: Int) {
    Snackbar.make(findViewById(android.R.id.content), resId, Snackbar.LENGTH_LONG).show()
}

fun Activity.canReadExternalStorage(): Boolean {
    return Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN
        || requestPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
}

fun Activity.requestPermission(permission: String): Boolean {
    val state = ContextCompat.checkSelfPermission(this, permission)
    if (state == PackageManager.PERMISSION_GRANTED)
        return true;
    ActivityCompat.requestPermissions(this, arrayOf(permission), 1);
    return false;
}