package com.myplaycity.deviceanalyzer

import android.content.Context
import android.support.v7.widget.CardView
import android.util.AttributeSet
import android.view.View
import android.view.animation.AccelerateInterpolator
import android.view.animation.DecelerateInterpolator
import android.widget.Button
import com.myplaycity.deviceanalyzer.common.animateCompat

/**
 * Created by i.chiruhin@myplaycity.com on 1/6/16.
 */
class CardButton(context: Context, attrs: AttributeSet) : CardView(context, attrs) {

    private val button: Button
    private val progress: View

    private var clickListener: OnClickListener? = null

    init {
        View.inflate(context, R.layout.layout_card_button, this)
        setCardBackgroundColor(resources.getColor(R.color.primary))
        radius = 4 * context.resources.displayMetrics.density

        button = findViewById(R.id.button) as Button
        progress = findViewById(R.id.progress)

        val text = attrs.getAttributeResourceValue("http://schemas.android.com/apk/res/android", "text", 0)
        if (text != 0) button.setText(text)

        button.setOnClickListener {
            clickListener?.let { it.onClick(this) }
        }
    }

    override fun setOnClickListener(clickListener: OnClickListener?) {
        this.clickListener = clickListener
    }

    fun setBusy(isBusy: Boolean) {
        if (isBusy) {
            animateCompat().setInterpolator(AccelerateInterpolator()).scaleX(0f).withEndAction {
                progress.visibility = View.VISIBLE
                button.visibility = View.GONE
                animate().setInterpolator(DecelerateInterpolator()).scaleX(1f)
            }
        } else {
            animateCompat().setInterpolator(AccelerateInterpolator()).scaleX(0f).withEndAction {
                progress.visibility = View.GONE
                button.visibility = View.VISIBLE
                animate().setInterpolator(DecelerateInterpolator()).scaleX(1f)
            }
        }
    }
}