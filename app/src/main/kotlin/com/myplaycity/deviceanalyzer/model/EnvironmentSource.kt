package com.myplaycity.deviceanalyzer.model

import android.os.Build
import rx.Observable
import rx.schedulers.Schedulers
import java.io.File
import java.lang.reflect.Modifier

/**
 * Created by i.chiruhin@myplaycity.com on 1/9/16.
 */
class EnvironmentSource {

    fun attachTo(targer: File): Observable<Unit> {
        return Observable
            .fromCallable {
                val buildEnv = Build::class.java
                    .fields
                    .filter { Modifier.isStatic(it.modifiers) }
                    .map { field ->
                        field.name to when {
                            field.type == Array<String>::class.java -> {
                                (field.get(null) as Array<*>).joinToString()
                            }
                            else -> field.get(null)?.toString() ?: "<null>"
                        }
                    }
                    .map { "${it.first}=${it.second}" }

                val env = listOf("=== System environments ===")
                    .union(System.getenv().map { "${it.key}=${it.value}" })
                    .union(listOf("\n=== System properties ==="))
                    .union(System.getProperties().map { "${it.key}=${it.value}" })
                    .union(listOf("\n=== Build environments ==="))
                    .union(buildEnv)
                    .joinToString(separator = "\n")

                targer.appendText("\n\n" + env)
            }
            .subscribeOn(Schedulers.io())
    }
}