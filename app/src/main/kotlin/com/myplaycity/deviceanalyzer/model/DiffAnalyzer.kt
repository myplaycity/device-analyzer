package com.myplaycity.deviceanalyzer.model

import rx.Observable
import rx.schedulers.Schedulers
import java.io.File

/**
 * Created by i.chiruhin@myplaycity.com on 1/8/16.
 */
class DiffAnalyzer {

    fun diff(sample: File, compareSample: File, target: File): Observable<Unit> {
        return Observable
            .fromCallable {
                val recordsAfter = compareSample
                    .bufferedReader()
                    .useLines {
                        it.map { Record.fromString(it) }.toMapBy { it.path }
                    }

                val recordsBefore = sample
                    .bufferedReader()
                    .useLines {
                        it.map { Record.fromString(it) }.toMapBy { it.path }
                    }

                val changedRecords = recordsBefore
                    .values
                    .map {
                        val cf = recordsAfter[it.path]
                        when {
                            cf == null -> it.copy(state = Record.State.Deleted)
                            cf.length != it.length -> it.copy(state = Record.State.Changed)
                            else -> null
                        }
                    }
                    .filterNotNull()
                    .toMapBy { it.path }
                val recordsAdded = recordsAfter
                    .filterNot { recordsBefore.containsKey(it.key) }
                    .map { it.value.copy(state = Record.State.Added) }
                    .toMapBy { it.path }

                target
                    .printWriter()
                    .use { writer ->
                        changedRecords.forEach { writer.println(it.value) }
                        recordsAdded.forEach { writer.println(it.value) }
                    }
            }
            .subscribeOn(Schedulers.io())
    }
}