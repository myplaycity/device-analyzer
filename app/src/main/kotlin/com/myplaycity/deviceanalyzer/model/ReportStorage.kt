package com.myplaycity.deviceanalyzer.model

import com.dropbox.core.DbxRequestConfig
import com.dropbox.core.v2.DbxClientV2
import rx.Observable
import rx.schedulers.Schedulers
import java.io.File
import java.util.*

/**
 * Created by i.chiruhin@myplaycity.com on 1/6/16.
 */
class ReportStorage {

    private val dbxClient by lazy {
        val config = DbxRequestConfig("DiskAnalyzer", "" + Locale.getDefault())
        DbxClientV2(config, "Y3SVnwFOdJMAAAAAAAAFsypigGCFLzlEQNLSepephCJ6Sx8qQ_PrshWN2D7zbj0e")
    }

    fun upload(src: File, name: String): Observable<Unit> {
        return Observable
            .fromCallable {
                src.inputStream().use {
                    dbxClient.files.uploadBuilder("/$name").run(it)
                }
                Unit // TODO: убрать Unit
            }
            .subscribeOn(Schedulers.io())
    }
}