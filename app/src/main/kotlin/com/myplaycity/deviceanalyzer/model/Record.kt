package com.myplaycity.deviceanalyzer.model

/**
 * Created by i.chiruhin@myplaycity.com on 1/6/16.
 */
data class Record(
    val path: String,
    val length: Long,
    val state: State = State.Unknown) {

    override fun toString(): String {
        return "$state$delimiter$length$delimiter$path"
    }

    enum class State {
        Unknown, Changed, Deleted, Added
    }

    companion object {

        private val delimiter = ", "

        fun fromString(value: String): Record {
            val s = value.split(delimiter, limit = 3)
            return Record(s[2], s[1].toLong(), State.valueOf(s[0]))
        }
    }
}