package com.myplaycity.deviceanalyzer.model

import com.myplaycity.deviceanalyzer.common.MainScheduler
import rx.Observable
import rx.schedulers.Schedulers
import java.io.File
import java.util.*

/**
 * Created by i.chiruhin@myplaycity.com on 1/5/16.
 */
class AnalyzeService(
    private val platform: Platform,
    private val fileSource: FileSource = FileSource(),
    private val environmentSource: EnvironmentSource = EnvironmentSource(),
    private val diffAnalyzer: DiffAnalyzer = DiffAnalyzer(),
    private val reportStorage: ReportStorage = ReportStorage()) {

    private val mainScheduler = MainScheduler()

    fun getStatus(): Observable<Status> {
        return Observable
            .fromCallable {
                when (sampleFile.length()) {
                    0L -> Status.First
                    else -> Status.Next
                }
            }
            .subscribeOn(Schedulers.io())
            .observeOn(mainScheduler)
    }

    fun analyze(): Observable<Unit> {
        return Database(sampleFile)
            .addAll(fileSource.getFileSource())
            .map { Unit }
            .doOnError { platform.logError(it) }
            .observeOn(mainScheduler)
    }

    fun computeDiff(): Observable<Unit> {
        // TODO: перенести создание файла для храния второй копии сюда
        val tmp = File.createTempFile("analyzer_", null, platform.directory)

        return Database(File.createTempFile("analyzer_", null, platform.directory))
            .addAll(fileSource.getFileSource())
            .flatMap { diffAnalyzer.diff(sampleFile, it, tmp) }
            .filter { tmp.length() > 0 }
            .flatMap { environmentSource.attachTo(tmp) }
            .flatMap { reportStorage.upload(tmp, createReportName()) }
            .doOnError { platform.logError(it) }
            .observeOn(mainScheduler)
    }

    private fun createReportName() = "${platform.deviceName} (${UUID.randomUUID()}).txt"

    private val sampleFile: File
        get() = File(platform.directory, "sample.txt")

    enum class Status { First, Next }
}