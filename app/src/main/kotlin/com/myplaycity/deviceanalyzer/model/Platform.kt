package com.myplaycity.deviceanalyzer.model

import android.content.Context
import android.os.Build
import com.myplaycity.deviceanalyzer.BuildConfig
import com.splunk.mint.Mint
import java.io.File
import java.io.OutputStream

/**
 * Created by i.chiruhin@myplaycity.com on 1/6/16.
 */
class Platform(private val context: Context) {

    val deviceName: String
        get() = Build.MODEL

    val directory: File
        get() = context.filesDir

    fun execute(command: String): OutputStream {
        return Runtime.getRuntime().exec(command).outputStream
    }

    fun logError(e: Throwable) {
        if (!BuildConfig.DEBUG && e is Exception) Mint.logException(e)
    }
}