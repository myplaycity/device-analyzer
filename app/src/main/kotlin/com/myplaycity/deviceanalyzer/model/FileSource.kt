package com.myplaycity.deviceanalyzer.model

import com.myplaycity.deviceanalyzer.common.filterByPrev
import com.myplaycity.deviceanalyzer.common.orDefault
import rx.Observable
import rx.schedulers.Schedulers
import java.io.File

/**
 * Created by i.chiruhin@myplaycity.com on 1/7/16.
 */
class FileSource {

    fun getFileSource(vararg roots: File): Observable<Record> {
        return Observable
            .from(roots.map { it.absolutePath }.orDefault("/", "/mnt/sdcard/"))
            .flatMap { root ->
                Observable.from(Runtime
                    .getRuntime().exec("du -ak $root")
                    .inputStream.bufferedReader()
                    .lineSequence().asIterable())
            }
            .map { line ->
                val match = fileRegex.find(line)?.groups ?: throw Exception("Can't parse: '$line'")
                Record(match[2]!!.value, match[1]!!.value.toLong() * 1024)
            }
            .filter { it.length > 0 && !it.path.startsWith("/storage/emulated/legacy") }
            .filterByPrev(Record("", 0)) { prev, s -> !prev.path.startsWith(s.path) }
            .subscribeOn(Schedulers.io())
    }

    companion object {

        private val fileRegex = Regex("^(\\d+)\\s+(.+)$")
    }
}