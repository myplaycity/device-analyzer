package com.myplaycity.deviceanalyzer.model

import rx.Observable
import rx.subjects.PublishSubject
import java.io.File

/**
 * Created by i.chiruhin@myplaycity.com on 1/6/16.
 */
class Database(private val file: File) {

    fun addAll(items: Observable<Record>): Observable<File> {
        val writer = file.bufferedWriter()

        val completeSource = PublishSubject.create<File>()
        items
            .subscribe(
                { writer.appendln(it.toString()) },
                {
                    writer.close()
                    completeSource.onError(it)
                },
                {
                    writer.close()
                    completeSource.onNext(file)
                    completeSource.onCompleted()
                })
        return completeSource;
    }
}