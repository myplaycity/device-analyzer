package com.myplaycity.deviceanalyzer

import android.os.Bundle
import android.os.StrictMode
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.myplaycity.deviceanalyzer.common.canReadExternalStorage
import com.myplaycity.deviceanalyzer.common.showSnackbar
import com.myplaycity.deviceanalyzer.common.subscribeComplete
import com.myplaycity.deviceanalyzer.model.AnalyzeService
import com.myplaycity.deviceanalyzer.model.Platform
import com.splunk.mint.Mint

class MainActivity : AppCompatActivity() {

    val analyzer = AnalyzeService(Platform(this))

    lateinit var analyzerButton: CardButton
    lateinit var computeDiff: CardButton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportActionBar.title = "${getString(R.string.app_name)} (${BuildConfig.VERSION_NAME})"

        if (BuildConfig.DEBUG) StrictMode.enableDefaults()
        else Mint.initAndStartSession(this, "c61457d1");

        analyzerButton = findViewById(R.id.analyze) as CardButton
        computeDiff = findViewById(R.id.computeDiff) as CardButton

        updateUI()

        analyzerButton.setOnClickListener {
            if (!canReadExternalStorage())
                return@setOnClickListener

            analyzerButton.setBusy(true)
            computeDiff.visibility = View.GONE
            analyzer.analyze().subscribeComplete {
                showSnackbar(R.string.sample_created_successfully)

                analyzerButton.setBusy(false)
                updateUI()
            }
        }

        computeDiff.setOnClickListener {
            if (!canReadExternalStorage())
                return@setOnClickListener

            computeDiff.setBusy(true)
            analyzerButton.visibility = View.GONE
            analyzer.computeDiff()
                .subscribeComplete { e ->
                    if (e == null) showSnackbar(R.string.report_successful_created)
                    else showSnackbar(R.string.cant_create_report)

                    computeDiff.setBusy(false)
                    updateUI()
                }
        }
    }

    fun updateUI() {
        analyzer
            .getStatus()
            .subscribe {
                analyzerButton.visibility = View.VISIBLE
                when (it) {
                    AnalyzeService.Status.First -> computeDiff.visibility = View.GONE
                    AnalyzeService.Status.Next -> computeDiff.visibility = View.VISIBLE
                }
            }
    }
}